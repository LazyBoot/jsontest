﻿using System;
using System.IO;
using System.Linq;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var json = File.ReadAllText(@"weapon_points.json");
            var weaps = Weapons.FromJson(json);

            Console.WriteLine("Air weapons: \n-------------------------------\n");
            foreach (var weapon in weaps.Where(w => w.Value.Category == "Air"))
            {
                Console.WriteLine($"{weapon.Key}: {weapon.Value.Points}");
            }

            Console.WriteLine();
            Console.WriteLine("Ground weapons: \n-------------------------------\n");
            foreach (var weapon in weaps.Where(w => w.Value.Category == "Ground"))
            {
                Console.WriteLine($"{weapon.Key}: {weapon.Value.Points}");
            }
        }
    }
}
